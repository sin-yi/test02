﻿git hub
sinyie 
lousalme8058@gmail.com
repository0324


git lab
lousalme8058@gmail.com
repository0324
＊git lab repository private改public：
setting > general > Visibility, project features, permissions > Project visibility 

cd 指向
cd .. 回上一層
cd c: 到C槽
cd test/css 到test檔案夾裡的css檔案夾
cd (直接把視窗的檔案夾拉到命令提示字元) 
   指向這個資料夾(但要先到C槽或D槽)
ls 查詢資料內容
mkdir test 新增test資料夾
touch test.css 新增test.css檔案
Q 離開git log


＊git 環境設定
＊要在根目錄設定！

1.建立user email
git config --global user.email "電子郵件地址"

2.建立user name
git config --global user.name "使用者姓名"

3.查詢是否建立成功
git config --list


＊local repostory 環境設定
用來監控資料有無變更
1.先到指定資料夾
2.建立local repository：輸入git init 
3.在資料夾中把隱藏檔案打開為可視，便可以看到git建立的檔案夾跟檔案


＊追蹤檔案
全部檔案加入索引：git add . 
加入某一個檔案到索引：git add 檔名
檢查引索狀態：git status　
提交更新：git commit -m '修改紀錄命名'
查詢提交紀錄：git log
Q :從git log返回操作
＊每一次要把檔案提交更新前，都要記得把全部檔案加入引索，否則不會提交！
＊git 檔案加入引索時，要在同.git層(最上層)，如果在下一層的檔案夾裏操作，上層更改的檔案不會進入引索更新！


＊忽略檔案
新增忽略檔案表： touch .gitignore
忽略表裡的指令：
忽略檔案範例： index.html
全部的html檔都忽略：*.html
忽略資料夾： /檔案名稱
＊如果檔案或資料夾在加入忽略表之前就已經進入引索(git add)，之後就無法忽略了！



＊取消索引
全部檔案取消索引：git reset HEAD
單一檔案取消索引：git reset HEAD 檔名
＊如果檔案之前就提交過，之後就無法取消索引了！


＊取消commit
查詢版本：git log --oneline
查詢範例：
e12d8ef (HEAD -> master) add database.yml in config folder
85e7e30 add hello
657fce7 add container
abb4f43 update index page
cef6e40 create index page
cc797cd init commit
如果想拆掉最後一次的 Commit，可以使用「相對」或「絕對」的做法。「相對」的做法可以這樣：

$ git reset e12d8ef^
在最後面的那個 ^ 符號，每一個 ^ 符號表示「前一次」的意思，所以 e12d8ef^ 是指在 e12d8ef 這個 Commit 的「前一次」，如果是 e12d8ef^^ 則是往前兩次，以此類推。
不過如果要倒退五次，通常不會寫 e12d8ef^^^^^，而會寫成 e12d8ef~5。

以上是「相對」的方式。如果你很清楚想要把目前的狀態退回到哪個 Commit，可以直接指明：

$ git reset 85e7e30
它就會切換到 85e7e30 這個 Commit 的狀態，因為 85e7e30 剛好就是 e12d8ef 的前一次 Commit，以這個例子來說也會達到跟「拆掉最後一次的 Commit」一樣的效果

＊還原檔案（還原至最新一次的提交狀態commit-m）
還原單一檔案：git checkout 檔名
還原目錄跟索引到最新一次的提交commit-m：git reset--hard
＊如果檔案為取消索引(untracked file)狀態，還原資料時會略過，不會覆蓋或是刪除它！


＊檔案傳到遠端git hub repository
1.先到資料夾裡
2.在git hub建立一個新的new repository
3.回指令視窗打git clone 網址
4.在新建立的new repository 資料夾裡面增加檔案跟程式
5.推送到本地數據庫 git add.／ git commit -m '紀錄名稱'
6.推送到遠端數據庫：git push,輸入git hub 帳密



＊檔案分支
1.HEAD：目前所在位置指標
2.主線為master：還沒提交任何commit之前，不會出現master

瀏覽目前分支：git branch　
檔案回到先前的commit 紀錄：git checkout 'commit紀錄前4碼'
（commit紀錄碼可用git log 查詢）
回master所在commit（最新的commit）：git checkout master
創建分支：git branch 分支名稱
查看有多少分支：git branch
到達指定分支：git checkout 分支名稱
刪除分支：回master,輸入 git branch -d 分支名稱


＊在分支中做的任何修改也需要推送：git commit-m '修改紀錄命名',
才能切換git branch


＊合併分支
1.HEAD必須回到主分支(master)中合併分支
2.合併完分支還是會保留，隨時可以查看
合併分支：git merge '分支名稱'


＊合併分支衝突
當合併時發現有衝突，必須把衝突的語法解決，再加入索引，並推送一次紀錄
git add . 跟commit -m '紀錄名稱'


＊查詢標籤　
1.在推送完提交紀錄後，即可增加git tag 紀錄

新增標籤：git tag 標籤名稱
新增標籤＋備註：git tag -am "備註內容" 標籤名稱
查詢標籤：git tag
查詢標籤＋備註：git tag -n
刪除標籤：git tag -d 標籤名稱
切換到標籤的commit：git checkout 標籤名稱


＊暫存檔案
1.可以暫存多個檔案紀錄不被推送上去紀錄，這些檔案可以在各分支被打開，但是打開後
必須將其編輯並推送完，才可跳到其他分支
2.推送完該暫存檔案紀錄就會消失
3.多個暫存檔案紀錄存在時，還原暫存會打開最近的紀錄，而且必須把紀錄推送上去，
才能打開舊的

暫時儲存當前目錄：git stash
瀏覽git stash列表：git stash list
還原暫存(最新一次的)：git stash pop 
清除最新暫存：git stash drop
清除全部暫存：git stash clear


＊推送遠端分支到數據庫
查詢遠端數據庫：git remote
修改遠端預設主機名稱：git remote 預設主機名稱(預設為origin) 修改名稱
推送分支到遠端數據庫：git push 預設主機名稱(origin) 分支名稱


＊把檔案從遠端資料庫拉下來：git pull
1.先把遠端網址複製下來資料夾監聽：git clone 網址
2.若遠端檔案有更新,就可以用git pull 把更新的檔案拉下來


＊本地檔案更改後，git push到遠端資料夾遇到衝突（因為本地拉下來的為舊檔案，遠端檔案已有人修改），
必須git pull遠端檔案更新到本地時，會出現該訊息視窗：

# Please enter a commit message to explain why this merge is necessary,
# especially if it merges an updated upstream into a topic branch. 
# # Lines starting with '#' will be ignored, and an empty message aborts 
# the commit.

視窗跳出方法：
(參考網站：https://stackoverflow.com/questions/19085807/please-enter-a-commit-message-to-explain-why-this-merge-is-necessary-especially/47223056)

press "i"
on top above on #lines write your message
press "esc" button
write ":wq" (it will write in bottom automatically)
press enter


＊相信各位同學練習完 pull 時，可以知道他就是將遠端的分支將你的本地分支進行合併 merge 的動作，
但有時候你不希望 pull 下來導致自己的數據庫太亂又擔心有衝突時，可以先使用下面這個指令。

git fetch origin(遠端數據庫) branch1(遠端分支)

此時你的分支會多一個 FETCH_HEAD 的分支，這個就是遠端數據庫的分支，可以等到你看過沒問題後，
再合併 FETCH_HEAD 也 ok。



＊SSH key

電腦要產生SSH key，才能拉下遠端檔案，否則第一次拉下檔案時會被拒絕
錯誤訊息:
Permission denied (publickey).
fatal: Could not read from remote repository.

Please make sure you have the correct access rights
and the repository exists.


１.先建立金鑰(第一次才要做)
ssh-keygen -t rsa -b 2048 -C "lousalme8058@gmail.com"
如果沒有要設定驗證密碼，請不要輸入任何文字，直接輸入『 Enter』 鍵即可。
2.打cat ~/.ssh/id_rsa.pub
複製這段金鑰，貼到gitlab--setting，新增一組SSH key

匯入SSH key在gitHub或其他repository
執行以下命令就可以確認產生SSH連線所需要公開金鑰的內容。要用id_rsa.pub檔
$ cat ~/.ssh/id_rsa.pub
3.查詢金鑰
$ ssh-keygen
4.金鑰所在位置
C:\Users\user\.ssh
